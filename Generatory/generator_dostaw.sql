CREATE OR REPLACE FUNCTION wybierz_dostawce RETURN NUMBER IS
    v_min NUMBER;
    v_max NUMBER;
    o_result NUMBER;
    ile NUMBER;
BEGIN
    SELECT MAX(idDostawcy) INTO v_max FROM Dostawca;
    SELECT MIN(idDostawcy) INTO v_min FROM Dostawca;
    <<petla>>
    LOOP
      o_result := round(dbms_random.value(v_min, v_max));
      SELECT COUNT(*) INTO ile FROM Dostawca WHERE idDostawcy = o_result;
      EXIT petla WHEN ile = 1;
	END LOOP petla;
	RETURN o_result;
END wybierz_dostawce;
/

CREATE OR REPLACE FUNCTION wybierz_produkt RETURN NUMBER IS
    v_min NUMBER;
    v_max NUMBER;
    o_result NUMBER;
    ile NUMBER;
BEGIN
    SELECT MAX(idProduktu) INTO v_max FROM Produkt;
    SELECT MIN(idProduktu) INTO v_min FROM Produkt;
    <<petla>>
    LOOP
      o_result := round(dbms_random.value(v_min, v_max));
      SELECT COUNT(*) INTO ile FROM Produkt WHERE idProduktu = o_result;
      CONTINUE petla WHEN ile = 0;
      SELECT COUNT(*) INTO ile FROM Produkty_w_dostawie WHERE IDPRODUKTU = o_result;
      EXIT petla WHEN ile = 0;
	END LOOP petla;
	RETURN o_result;
END wybierz_produkt;
/

CREATE OR REPLACE PROCEDURE generuj_dostawy(poczatek IN DATE, koniec IN DATE, ile IN NUMBER) AS
    v_zrobiono NUMBER;
    v_id_dostawcy NUMBER;
    v_id_dostawy NUMBER;
    v_id_produktu NUMBER;
    v_ile_pozycji NUMBER;
    v_data_dostawy DATE;
    v_cena_jednostkowa NUMBER;
    v_ilosc NUMBER;
    v_biezaca_pozycja NUMBER;
    v_nr_faktury VARCHAR2(255);
BEGIN
	v_zrobiono := 0;
	<<dostawy>>
	LOOP
		v_id_dostawcy := wybierz_dostawce();
		v_data_dostawy :=  to_date( trunc( round(dbms_random.value(TO_CHAR(poczatek,'J'),TO_CHAR(koniec,'J')))),'J');
		v_nr_faktury := TO_CHAR(v_data_dostawy, 'MMDDD')||'/'||TO_CHAR(v_id_dostawcy+83)||'/'||TO_CHAR(v_data_dostawy, 'YYYY');
		INSERT INTO Dostawa (idDostawcy, DataDostawy, NrFaktury) VALUES (v_id_dostawcy, v_data_dostawy, v_nr_faktury) RETURNING IdDostawy INTO v_id_dostawy;
		v_ile_pozycji := round(dbms_random.value(2, 10));
		v_biezaca_pozycja := 1;
		DELETE FROM produkty_w_dostawie WHERE 1=1;
		<<pozycje>>
		LOOP
			v_id_produktu := wybierz_produkt();
			v_cena_jednostkowa := dbms_random.value(1, 500);
			v_ilosc := round(dbms_random.value(1, 100));
			INSERT INTO pozDostawy(Pozycja, IdDostawy, IdProduktu, CenaJednostkowaProd, Ilosc) VALUES (v_biezaca_pozycja, v_id_dostawy, v_id_produktu, v_cena_jednostkowa, v_ilosc);
			INSERT INTO Produkty_w_dostawie VALUES (v_id_produktu);
			v_biezaca_pozycja := v_biezaca_pozycja + 1;
			EXIT pozycje WHEN v_biezaca_pozycja > v_ile_pozycji;
		END LOOP pozycje;
		DELETE FROM Produkty_w_dostawie WHERE 1=1;
		v_zrobiono := v_zrobiono + 1;
		EXIT dostawy WHEN v_zrobiono > ile;
	END LOOP dostawy;
END generuj_dostawy;
/
