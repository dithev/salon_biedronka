CREATE OR REPLACE FUNCTION wybierz_klienta(p_data_wizyty IN DATE) RETURN NUMBER IS
    v_min NUMBER;
    v_max NUMBER;
	v_1_klient DATE;
    o_result NUMBER;
    ile NUMBER;
BEGIN
	SELECT MIN(DATAREJESTRACJI) INTO v_1_klient FROM Klient;
	IF v_1_klient > p_data_wizyty THEN 
		RAISE_APPLICATION_ERROR(-20001, 'Data wizyty nie moze byc wczesniejsza niz data zarejestrowania wszystkich klientow.');
	END IF;
    SELECT MAX(idKlienta) INTO v_max FROM Klient;
    SELECT MIN(idKlienta) INTO v_min FROM Klient;
    <<petla>>
    LOOP
      o_result := round(dbms_random.value(v_min, v_max));
      SELECT COUNT(*) INTO ile FROM Klient WHERE idKlienta = o_result AND DATAREJESTRACJI < p_data_wizyty;
      EXIT petla WHEN ile = 1;
    END LOOP petla;
    RETURN o_result;
END wybierz_klienta;
/

CREATE OR REPLACE FUNCTION wybierz_usluge RETURN NUMBER IS
    v_min NUMBER;
    v_max NUMBER;
    o_result NUMBER;
    ile NUMBER;
BEGIN
    SELECT MAX(idUslugi) INTO v_max FROM Usluga;
    SELECT MIN(idUslugi) INTO v_min FROM Usluga;
    <<petla>>
    LOOP
      o_result := round(dbms_random.value(v_min, v_max));
      SELECT COUNT(*) INTO ile FROM Usluga WHERE idUslugi = o_result;
      CONTINUE petla WHEN ile = 0;
      SELECT COUNT(*) INTO ile FROM Usluga_w_wizycie WHERE IDUSLUGI = o_result;
      EXIT petla WHEN ile = 0;
    END LOOP petla;
    INSERT INTO Usluga_w_wizycie(idUslugi) VALUES (o_result);
    RETURN o_result;
END wybierz_usluge;
/

CREATE OR REPLACE FUNCTION wybierz_pracownika RETURN NUMBER IS
    v_min NUMBER;
    v_max NUMBER;
    o_result NUMBER;
    ile NUMBER;
BEGIN
    SELECT MAX(idPracownika) INTO v_max FROM Pracownik;
    SELECT MIN(idPracownika) INTO v_min FROM Pracownik;
    <<petla>>
    LOOP
      o_result := round(dbms_random.value(v_min, v_max));
      SELECT COUNT(*) INTO ile FROM Pracownik WHERE idPracownika = o_result;
      EXIT petla WHEN ile = 1;
    END LOOP petla;
    RETURN o_result;
END wybierz_pracownika;
/

CREATE OR REPLACE PROCEDURE generuj_wizusl(p_id_wizyty IN NUMBER, p_ile_uslug IN NUMBER, p_data_wizyty IN DATE) AS
    v_zrobiono NUMBER;
    v_id_uslugi NUMBER;
    v_numer_kolejny NUMBER;
    v_koszt NUMBER;
    v_id_pracownika NUMBER;
    v_TN CHAR;
BEGIN
	DELETE FROM Usluga_w_wizycie WHERE 1=1;
	v_numer_kolejny := 1;
	<<uslugi>>
	LOOP
		v_id_uslugi := wybierz_usluge();
		v_id_pracownika := wybierz_pracownika();
		v_koszt := dbms_random.value(150, 500);
		v_TN := CASE WHEN p_data_wizyty > SYSDATE THEN 'N' ELSE 'T' END;
		INSERT INTO WizUsl (IdUslugi, IdWizyty, IdPracownika, NumerKolejny, CzyZrealizowana, Koszt) VALUES (v_id_uslugi, p_id_wizyty, v_id_pracownika, v_numer_kolejny, v_TN, v_koszt);
		v_numer_kolejny := v_numer_kolejny + 1;
		EXIT uslugi WHEN v_numer_kolejny > p_ile_uslug;
	END LOOP uslugi;
END generuj_wizusl;   
/

CREATE OR REPLACE PROCEDURE generuj_wizyty(poczatek IN DATE, koniec IN DATE, ile IN NUMBER) AS
    v_zrobiono NUMBER;
    v_id_wizyty NUMBER;
    v_data_wizyty TIMESTAMP;
    v_id_klienta NUMBER;
    v_ile_uslug NUMBER;
BEGIN
	v_zrobiono := 0;
	<<wizyty>>
	LOOP
		v_data_wizyty := to_timestamp( to_char(round(dbms_random.value(TO_CHAR(poczatek,'J'),TO_CHAR(koniec,'J'))))||'.'||to_char(round(dbms_random.value(39600, 64800))),'J.SSSSS');
		v_id_klienta := wybierz_klienta(v_data_wizyty);
		INSERT INTO TerminWiz (idKlienta, DataWizyty) VALUES (v_id_klienta, v_data_wizyty) RETURNING IdWizyty INTO v_id_wizyty;
		v_ile_uslug := round(dbms_random.value(1, 5));
		generuj_wizusl(v_id_wizyty, v_ile_uslug, v_data_wizyty);
		v_zrobiono := v_zrobiono + 1;
		EXIT wizyty WHEN v_zrobiono > ile;
	END LOOP wizyty;
END generuj_wizyty;
/

