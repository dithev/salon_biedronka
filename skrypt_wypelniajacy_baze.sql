INSERT INTO Pracownik (Imie, Nazwisko, DataZatrudnienia, Zarobki, Adres, Uwagi) VALUES ('Anna', 'Kowalska', TO_DATE('2010-11-15','RRRR-MM-DD'), 1000.00, 'ul. Kolejowa 10 Warszawa 01-321', NULL);
INSERT INTO Pracownik (Imie, Nazwisko, DataZatrudnienia, Zarobki, Adres, Uwagi) VALUES ('Dagmara', 'Kokon', TO_DATE('2010-12-01','RRRR-MM-DD'), 900.00, 'ul. Powstańców Śląskich 129A Warszawa 01-466', NULL);
INSERT INTO Pracownik (Imie, Nazwisko, DataZatrudnienia, Zarobki, Adres, Uwagi) VALUES ('Agnieszka', 'Jasinska', TO_DATE('2013-04-18','RRRR-MM-DD'), 800.00, 'ul. Radiowa 16 Warszawa 01-485', NULL);
INSERT INTO Pracownik (Imie, Nazwisko, DataZatrudnienia, Zarobki, Adres, Uwagi) VALUES ('Adam', 'Abacki', TO_DATE('2014-09-20','RRRR-MM-DD'), 800.00, 'ul. Synów Pułku 7/65 Warszawa 01-354', NULL);
INSERT INTO Pracownik (Imie, Nazwisko, DataZatrudnienia, Zarobki, Adres, Uwagi) VALUES ('Piotr', 'Domek', TO_DATE('2014-09-20','RRRR-MM-DD'), 800.00, 'ul. Góralska 2/38 Warszawa 01-112', NULL);
INSERT INTO Pracownik (Imie, Nazwisko, DataZatrudnienia, Zarobki, Adres, Uwagi) VALUES ('Weronika', 'Grabińska', TO_DATE('2017-08-03','RRRR-MM-DD'), 700.00, 'ul. Wolska 52 Warszawa 01-229', 'Studentka na studiach dziennych. Zmiany tylko w weekend');

INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Elżbieta','Kwiatkowska', TO_DATE('2017-11-11', 'RRRR-MM-DD'), 111-111-111, NULL);	
INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Agata','Balicka', TO_DATE('2017-11-11', 'RRRR-MM-DD'), 222-222-222, NULL);		
INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Katarzyna','Wiśniewska', TO_DATE('2017-11-11', 'RRRR-MM-DD'), 333-333-333, NULL);		
INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Marta','Nowicka', TO_DATE('2017-11-15', 'RRRR-MM-DD'), 444-444-444, NULL);		
INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Agata','Biedrzycka', TO_DATE('2017-12-03', 'RRRR-MM-DD'), 555-555-555, NULL);
INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Iwona','Lewandowska', TO_DATE('2017-12-13', 'RRRR-MM-DD'), 666-666-666, NULL);	
INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Mariola','Gabor', TO_DATE('2017-12-28', 'RRRR-MM-DD'), 777-777-777, NULL);	
INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Beata','Chmiel', TO_DATE('2018-01-19', 'RRRR-MM-DD'), 888-888-888, NULL);		
INSERT INTO Klient (Imie, Nazwisko, DataRejestracji, TelefonKontaktowy, Uwagi) VALUES ('Barbara','Babacka', TO_DATE('2018-01-15', 'RRRR-MM-DD'), 999-999-999, NULL);	

INSERT INTO Dostawca (IdDostawcy, NazwaKontrahenta, AdresKontrahenta, MiejscowoscKontrahenta, TelefonKontaktowy, Uwagi) VALUES (1, 'Miejska Piwniczka Hurtownia Kosmetyczna', 'ul. Kwiatowa 3', '05-800 Pruszków', 123-111-222, NULL);
INSERT INTO Dostawca (IdDostawcy, NazwaKontrahenta, AdresKontrahenta, MiejscowoscKontrahenta, TelefonKontaktowy, Uwagi) VALUES (2, 'Ponadziemski Raj Akcesoria Kosmetyczne', 'ul. Biała 8', '05-120 Legionowo', 123-222-222, NULL);

INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (1, 'Tipsy', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (2, 'Żel', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (3, 'Pilnik do paznokci', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (4, 'Lakier do paznokci', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (5, 'Zmywacz do paznokci', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (6, 'Maseczka oczyszczająca', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (7, 'Płyn do zmywania maseczki', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (8, 'Henna', 'Barwnik do brwi');
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (9, 'Pęseta', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (10, 'Sterylne igły', 'Do jednorazowego użytku');
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (11, 'Aparat do przebijania uszu', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (12, 'Spirytus salicylowy', NULL);
INSERT INTO Produkt (IdProduktu, Nazwa, Uwagi) VALUES (13, 'Waciki', NULL);

INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (1, 4, 'Nakładanie lakieru', '1000', 10.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (2, 5, 'Zmywanie paznokci', '0500', 1.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (3, 3, 'Piłowanie paznokci', '0500', 5.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (4, 6, 'Maseczka', '1500', 10.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (5, 8, 'Nakladanie henny', '1000', 15.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (6, 13, 'Zmywanie henny', '0200', 10.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (7, 9, 'Depilacja brwi', '1000', 10.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (8, 12, 'Dezynfekcja', '0100', 2.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (9, 11, 'Przekłuwanie uszu', '0030', 18.00);
INSERT INTO Zabieg (IdZabiegu, IdProduktu, Nazwa, CzasTrwania, Cena) VALUES (10, 10, 'Piercing', '1000', 20.00);

INSERT INTO Usluga (IdUslugi, IdPracownika, Nazwa) VALUES (1, 1, 'Manicure');
INSERT INTO Usluga (IdUslugi, IdPracownika, Nazwa) VALUES (2, 1, 'Pedicure');
INSERT INTO Usluga (IdUslugi, IdPracownika, Nazwa) VALUES (3, 2, 'Henna');
INSERT INTO Usluga (IdUslugi, IdPracownika, Nazwa) VALUES (4, 3, 'Depilacja - wąsik lub brwi');
INSERT INTO Usluga (IdUslugi, IdPracownika, Nazwa) VALUES (5, 4, 'Przekłuwanie uszu');
INSERT INTO Usluga (IdUslugi, IdPracownika, Nazwa) VALUES (6, 6, 'Piercing');
INSERT INTO Usluga (IdUslugi, IdPracownika, Nazwa) VALUES (7, 4, 'Maseczka');

INSERT INTO TerminWiz (IdKlienta, DataWizyty, Uwagi) VALUES (1, TO_DATE('2017-11-11', 'RRRR-MM-DD'), NULL);
INSERT INTO TerminWiz (IdKlienta, DataWizyty, Uwagi) VALUES (3, TO_DATE('2017-11-11', 'RRRR-MM-DD'), NULL);
INSERT INTO TerminWiz (IdKlienta, DataWizyty, Uwagi) VALUES (5, TO_DATE('2017-12-30', 'RRRR-MM-DD'), NULL);
INSERT INTO TerminWiz (IdKlienta, DataWizyty, Uwagi) VALUES (6, TO_DATE('2017-12-13', 'RRRR-MM-DD'), NULL);
INSERT INTO TerminWiz (IdKlienta, DataWizyty, Uwagi) VALUES (7, TO_DATE('2017-12-28', 'RRRR-MM-DD'), NULL);
INSERT INTO TerminWiz (IdKlienta, DataWizyty, Uwagi) VALUES (9, TO_DATE('2018-01-16', 'RRRR-MM-DD'), NULL);
INSERT INTO TerminWiz (IdKlienta, DataWizyty, Uwagi) VALUES (8, TO_DATE('2018-01-23', 'RRRR-MM-DD'), NULL);

INSERT INTO Dostawa (IdDostawcy, NrFaktury, DataDostawy, Uwagi) VALUES (1, '1735/FR/1/2017', TO_DATE('2017-12-06', 'RRRR-MM-DD'), NULL);
INSERT INTO Dostawa (IdDostawcy, NrFaktury, DataDostawy, Uwagi) VALUES (2, '2957/2017', TO_DATE('2017-12-13', 'RRRR-MM-DD'), NULL);
INSERT INTO Dostawa (IdDostawcy, NrFaktury, DataDostawy, Uwagi) VALUES (1, '1738/FR/1/2017', TO_DATE('2017-12-20', 'RRRR-MM-DD'), NULL);

INSERT INTO PozDostawy (IdDostawy, Pozycja, IdProduktu, CenaJednostkowaProd, Ilosc) VALUES (1, 1, 3, 2.00, 10);
INSERT INTO PozDostawy (IdDostawy, Pozycja, IdProduktu, CenaJednostkowaProd, Ilosc) VALUES (1, 2, 4, 5.00, 10);
INSERT INTO PozDostawy (IdDostawy, Pozycja, IdProduktu, CenaJednostkowaProd, Ilosc) VALUES (2, 1, 5, 10.00, 2);
INSERT INTO PozDostawy (IdDostawy, Pozycja, IdProduktu, CenaJednostkowaProd, Ilosc) VALUES (2, 2, 2, 15.00, 10);
INSERT INTO PozDostawy (IdDostawy, Pozycja, IdProduktu, CenaJednostkowaProd, Ilosc) VALUES (3, 1, 12, 6.00, 3);
INSERT INTO PozDostawy (IdDostawy, Pozycja, IdProduktu, CenaJednostkowaProd, Ilosc) VALUES (3, 2, 11, 150.00, 2);

INSERT INTO WizUsl (IdUslugi, IdWizyty, IdPracownika, NumerKolejny, CzyZrealizowana, Koszt) VALUES (1, 1, 1, 1, 'T', 200.00);
INSERT INTO WizUsl (IdUslugi, IdWizyty, IdPracownika, NumerKolejny, CzyZrealizowana, Koszt) VALUES (2, 1, 1, 2, 'T', 210.00);
INSERT INTO WizUsl (IdUslugi, IdWizyty, IdPracownika, NumerKolejny, CzyZrealizowana, Koszt) VALUES (3, 2, 2, 1, 'T', 250.00);
INSERT INTO WizUsl (IdUslugi, IdWizyty, IdPracownika, NumerKolejny, CzyZrealizowana, Koszt) VALUES (4, 2, 3, 2, 'T', 120.00);
INSERT INTO WizUsl (IdUslugi, IdWizyty, IdPracownika, NumerKolejny, CzyZrealizowana, Koszt) VALUES (7, 2, 4, 3, 'T', 100.00);

INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (1, 2, 1);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (1, 3, 2);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (1, 1, 3);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (2, 2, 1);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (2, 3, 2);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (2, 1, 3);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (3, 5, 1);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (3, 6, 2);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (4, 7, 1);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (5, 8, 1);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (5, 9, 2);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (6, 8, 1);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (6, 10, 2);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (7, 4, 1);
INSERT INTO UslZab (IdUslugi, IdZabiegu, NumerKolejny) VALUES (7, 4, 2);




