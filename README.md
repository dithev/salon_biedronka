# Salon Biedronka

##### INSTALACJA BAZY DANYCH: 

###### Do uruchomienia bazy danych używamy kolejnych plików:
 
`skrypt_tworzacy_baze.sql` - tworzy tabele, opisujące salon, wraz z relacjami.   
`skrypt_wypelniajacy_baze.sql` - uzupełnia tabele przykładowymi danymi.   
`generator_dostaw.sql` – tworzy procedurę i funkcje umożliwiające wygenerowanie nowych dostaw.   
`generator_wizyt.sql` – tworzy funkcje i procedury umożliwiające wygenerowanie nowych wizyt.   
`wywolanie_generatorow.sql` – przykładowy sposób w jaki można uruchomić generatory.   

##### Żeby cokolwiek dodać do bazy trzeba ją najpierw uruchomić!

###### Do usunięcia bazy danych wywołujemy plik:

`skrypt_usuwajacy_baze.sql`

###### Wszystkie pliki należy wykonywać w podłączonej do serwera Oracle aplikacji SQL Developer w następujący sposób.

1. Za pomocą  `File -> Open`  otworzyć plik z rozszerzeniem `.sql`
2. Ikonką `Run Script` lub przyciskiem `F5`.

###### Lub innej analogicznej aplikacji umożliwiającej bezpośredni dostęp do serwera Oracle, dającej możliwość wykonania skryptów SQL.