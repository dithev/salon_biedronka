CREATE OR REPLACE VIEW V_PozDostawy AS
SELECT
   ds.NrFaktury,
   ds.IDDOSTAWY,
   ds.DataDostawy,
   PD.Pozycja,
   pt.IDPRODUKTU,
   pt.Nazwa,
   PD.CenaJednostkowaProd,
   PD.Ilosc,
   PD.CENAJEDNOSTKOWAPROD * PD.ILOSC Wartosc,
   dst.IDDOSTAWCY,
   dst.NazwaKontrahenta,
   dst.AdresKontrahenta,
   dst.MiejscowoscKontrahenta
FROM
   PozDostawy PD
   INNER JOIN Dostawa ds ON ds.IDDOSTAWY = PD.IDDOSTAWY
   INNER JOIN Dostawca dst ON dst.IDDOSTAWCY = ds.IDDOSTAWCY
   INNER JOIN Produkt pt ON pt.IDPRODUKTU = PD.IDPRODUKTU
WHERE
   1=1
ORDER BY ds.IDDOSTAWY, ds.DataDostawy, PD.POZYCJA;