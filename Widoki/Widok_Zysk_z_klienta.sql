CREATE OR REPLACE VIEW V_Zysk_z_klienta AS
SELECT
   kl.IDKLIENTA,
   kl.Imie Imie_Klienta,
   kl.Nazwisko Nazwisko_Klienta,
   us.NAZWA,
   us.IDUSLUGI,
   SUM(WU.KOSZT) Zysk,
   COUNT(WU.KOSZT) Liczba_wizyt
FROM
   WizUsl WU
   INNER JOIN Usluga us ON us.IDUSLUGI = WU.IDUSLUGI
   INNER JOIN TerminWiz tw ON  tw.IDWIZYTY = WU.IDWIZYTY
   INNER JOIN Klient kl ON kl.IDKLIENTA = tw.IDKLIENTA
WHERE
   1=1 AND WU.CZYZREALIZOWANA = 'T'
GROUP BY kl.IDKLIENTA, kl.Imie, kl.Nazwisko, us.NAZWA, us.IDUSLUGI
ORDER BY kl.Nazwisko, kl.Imie;

   