CREATE OR REPLACE VIEW V_WizUsl AS
SELECT
   WU.IDWIZYTY,
   kl.DataRejestracji,
   tw.DataWizyty,
   kl.IDKLIENTA,
   kl.Imie Imie_Klienta,
   kl.Nazwisko Nazwisko_Klienta,
   WU.NUMERKOLEJNY,
   us.NAZWA,
   WU.KOSZT,
   WU.CZYZREALIZOWANA,
   pr.IDPRACOWNIKA,
   pr.Imie Imie_Pracownika,
   pr.Nazwisko Nazwisko_Pracownika
FROM
   WizUsl WU
   INNER JOIN Usluga us ON us.IDUSLUGI = WU.IDUSLUGI
   INNER JOIN TerminWiz tw ON  tw.IDWIZYTY = WU.IDWIZYTY
   INNER JOIN Klient kl ON kl.IDKLIENTA = tw.IDKLIENTA
   INNER JOIN Pracownik pr ON pr.IDPRACOWNIKA = us.IDPRACOWNIKA
WHERE
   1=1 AND DataWizyty BETWEEN '01-FEB-18' AND '28-FEB-18'
GROUP BY tw.DataWizyty, kl.DataRejestracji, WU.IDWIZYTY, kl.IDKLIENTA, kl.IMIE, kl.NAZWISKO, WU.NUMERKOLEJNY, us.NAZWA, WU.KOSZT, WU.CZYZREALIZOWANA, pr.IDPRACOWNIKA, pr.IMIE, pr.Nazwisko
ORDER BY tw.DATAWIZYTY;


   